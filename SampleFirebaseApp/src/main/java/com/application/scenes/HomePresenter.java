package com.application.scenes;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;

public class HomePresenter
{


    public HomePresenter()
    {

    }

    public Node getView()
    {
        try {
          Node node=  FXMLLoader.load(getClass().getResource("home.fxml"));
          return node;
        } catch (IOException e) {
            e.printStackTrace();
        }
return null;

    }
}
