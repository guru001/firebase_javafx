package com.application.scenes;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;

public class SignupPresenter
{
    public SignupPresenter()
    {

    }
    public Node getView()
    {
        try {
            Node node=  FXMLLoader.load(getClass().getResource("signup.fxml"));
            return node;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }
}
