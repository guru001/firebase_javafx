package com.application;

import com.application.scenes.HomePresenter;
import com.application.scenes.SignupPresenter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXDrawersStack;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application
{

    public static void main(String args[])
    {

launch();

    }
@Override public void init()
{
    System.out.println("Init");
}

    @Override
    public void start(Stage primaryStage)  {
        try{
            HomePresenter home = new HomePresenter();
            SignupPresenter signup = new SignupPresenter();
            JFXButton press= new JFXButton("Click ");
            StackPane pane = new StackPane(signup.getView());


            JFXDrawersStack jfxDrawersStack= new JFXDrawersStack();
            JFXDrawer jfxDrawer= new JFXDrawer();
            jfxDrawersStack.setContent(pane);

            StackPane leftDrawerPane = new StackPane();
            leftDrawerPane.getStyleClass().add("red-400");
            leftDrawerPane.getChildren().add(new JFXButton("Left Content"));
            jfxDrawer.setSidePane(leftDrawerPane);
            jfxDrawer.setDefaultDrawerSize(150);
            jfxDrawer.setOverLayVisible(false);
            jfxDrawer.setResizableOnDrag(true);

            press.addEventHandler(MouseEvent.MOUSE_PRESSED,e->{
                jfxDrawersStack.toggle(jfxDrawer);
            });
            Scene scene = new Scene(jfxDrawersStack,350,600);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();

        }catch(Exception i)
        {
            i.printStackTrace();
        }

    }
}
